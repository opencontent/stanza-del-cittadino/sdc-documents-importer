FROM python:3.12.3-alpine3.19

ENV PYTHONUNBUFFERED 1

RUN apk add --no-cache tzdata

ENV TZ=Europe/Rome

RUN mkdir -p /app

WORKDIR /app

ADD requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

COPY . .

RUN chmod +x /app/*.py







