#Importatore documenti

Script per l'importazione dei documenti tramite API della stanza del cittadino

## Parametri

Per poter utilizzare lo script è necessario definire il parametro `API_URL` all'interno dello script

* `API_URL`: Url base alle API (esempio: http://devsdc.opencontent.it/comune-di-bugliano/api)


Durante l'esecuzione verrà generato in output un file csv contenente gli identificativi del documento e della cartella, Document ID
e Folder ID rispettivamente. 

## Csv

Il file csv da importare dovrà avere le seguenti colonne:

*   Folder Name (obbligatorio): Cartella in cui il cittadino troverà il documento.
*   Fiscal Code (obbligatorio): Codice fiscale del cittadino per cui è pubblicato il documento
*   Mime-type (obbligatorio): [Tipo del documento](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types)
*   Original Filename (obbligatorio): Nome del file che il cittadino scarica sul proprio computer. E' importante che sia specificato un nome di file con estensione.
*   File Address (obbligatorio): Indirizzo al quale il documento viene scaricato. Il cittadino non avrà visibilità di questo indirizzo, questo verrà utilizzato dalla Stanza del Cittadino in modo trasparente
*   Title (obbligatorio): Titolo del documento mostrato sull'interfaccia web
*   Store (obbligatorio): Se FALSE il documento non viene caricato sulla stanza del cittadino, viene conservato il suo indirizzo originale e viene scaricato al volo dal FILE ADDRESS in caso di download da parte del cittadino. Se TRUE il file viene caricato sulla stanza del cittadino e viene conservato fino a EXPIRE AT
*   Description: Descrizione opzionale del documento
*   Expire At: ISO8601
*   Validity Begin: ISO8601
*   Validity End: ISO8601
*   Document ID: Identificativo documento (se esistente)
*   Folder ID: Identificativo cartella (se esistente)


## Esecuzione

Installare virtualenv

    virtualenv venv --python python3.7
    source venv/bin/python
 
Installare le dipendenze:
    
    pip install -r requirements.txt
    
Esecuzione 

Di seguito sono elencati gli argomenti (obbligatori e non) che devono essere passati all'avvio dello script.

| Argomento         | Valore di default | Obbligatorio  | Tipo        | Descrizione                                               |
|-------------------|-------------------|---------------|-------------|-----------------------------------------------------------|
| `api_url`         | ---               | SI            | `str`       | API url della stanza del cittadino                        |
| `usename`         | ---               | SI            | `str`       | Username utilizzato per l'autenticazione alle API         |
| `password`        | ---               | SI            | `str`       | Password utilizzata per l'autenticazione alle API         |
| `file`            | ---               | SI            | `str`       | Nome del file csv contenente i documenti da importare     |
| `delimiter`       | ,                 | NO            | `str`       | Delimitatore csv                                          |
| `skip-rows`       | 0                 | NO            | `int`       | Numero di righe da saltare                                |
| `output-file`     | output.csv        | NO            | `str`       | Nome del file generato in output dallo script             |
| `force-update`    | `False`           | NO            | `bool`      | Forzare l'aggiornamento del documento se già esistente    |

Quindi il comando da eseguire è il seguente:

    python import.py <username> <password> <file> --skip-rows <skip-rows> --output-file <output-file> --force-update
    
## Aggiornamento documenti

Per aggiornare i documenti precedentemente importati è possibile

* Popolare la colonna `DocumentID` all'interno del file -csv. In questo caso l'aggiornamento sarà automatico
* Lanciare lo script con il flag `--force-update`

E'inoltre possibile aggiungere successivamente nuovi documenti ad una cartella preesistente, non è necessario popolare i 
campi `FolderID` e `DocumentID` (se ne consiglia tuttavia l'uso), occorre verificare che non
 esista già un documento con lo stesso nome, in tal caso il documento non verrà importato (se ne può forzare l'aggiornamento
  utilizzando il flag `--force-update` come sopra indicato).

## Docker

Per l'esecuzione attraverso docker è sufficiente modificare la riga `command` all'interno del file `docker-compose.yml`
inserendo i parametri di autenticazione e gli eventuali parametri opzionali precedentemente descritti.

    `docker-compose up -d`